# CRAN comments

## Bump version to 0.5

+ Updated R-dependency to 4.3
+ Removal of dependency on `rgdal`
+ General code overhaul

### Comment

Unfortunately, I was unable to address the the issues in the previous version in a timely fashion and the package is now archived on CRAN. I hope it is possible to revive the package on CRAN.

## R CMD check

### Test environments

+ Ubuntu Ubuntu 23.04, R version 4.3 (Local)
+ rocker/r-base:latest (Debian based)
+ Windows Server 2022, R-release, 32/64 bit (Rhub; windows-x86_64-release)
+ macOS R-release (https://mac.r-project.org/macbuilder/submit.html)
+ Windows Server 2022, R-devel, 64 bit (R-hub; windows-x86_64-devel)
+ Ubuntu Linux 20.04.1 LTS, R-devel, GCC (R-hub; ubuntu-gcc-release)
+ Fedora Linux, R-devel, clang, gfortran (R-hub; fedora-clang-devel)

  
### Results

Except as noted below, all tests gave no ERRORs, NOTEs, or WARNINGs. 


#### Notes

`* checking CRAN incoming feasibility ...  NOTE` was found for 

+ Windows Server 2022, R-release, 32/64 bit (Rhub; windows-x86_64-release)
+ Windows Server 2022, R-devel, 64 bit (R-hub; windows-x86_64-devel)
+ Ubuntu Linux 20.04.1 LTS, R-devel, GCC (R-hub; ubuntu-gcc-release)
+ Fedora Linux, R-devel, clang, gfortran (R-hub; fedora-clang-devel)

My understanding is that there may be some mismatch between e-mail addresses.


```
* checking HTML version of manual ... NOTE
Skipping checking HTML validation: no command 'tidy' found
Skipping checking math rendering: package 'V8' unavailable
```

was found for 

+ Ubuntu Linux 20.04.1 LTS, R-devel, GCC (R-hub; ubuntu-gcc-release)
+ Fedora Linux, R-devel, clang, gfortran (R-hub; fedora-clang-devel)
+ Windows Server 2022, R-devel, 64 bit (R-hub; windows-x86_64-devel) - only missing V8 package
+ Windows Server 2022, R-release, 32/64 bit (Rhub; windows-x86_64-release) - only missing V8 package

I am unsure how to solve this since I cannot update the R-hub docker images. Also, on the local machine, testing with the rocker/r-base:latest or macOS R-release the note does not exist, making it likely that this note indeed is related to R-hub.

The `Skipping checking math rendering: package 'V8' unavailable` can be fixed for:

+ Fedora Linux, R-devel, clang, gfortran (R-hub; fedora-clang-devel)
+ Windows Server 2022, R-devel, 64 bit (R-hub; windows-x86_64-devel) - only missing V8 package
+ Windows Server 2022, R-release, 32/64 bit (Rhub; windows-x86_64-release) - only missing V8 package

by adding:

```
SystemRequirements:
  v8: libv8-dev (deb) or v8-314-devel (rpm)
Suggests:
    ...
    V8
```

to the DESCRIPTION. However, this causes the error

```
* checking HTML version of manual ...Warning in file(con, "r") :
  cannot open file '/usr/lib/R/doc/html/katex/katex.js': No such file or directory
```
on Ubuntu Linux 20.04.1 LTS, R-devel, GCC (R-hub; ubuntu-gcc-release).


#### Notes relevant for Windows only

The following notes are raised on both:

+ Windows Server 2022, R-release, 32/64 bit (Rhub; windows-x86_64-release)
+ Windows Server 2022, R-devel, 64 bit (R-hub; windows-x86_64-devel)

```
* checking for non-standard things in the check directory ... NOTE
Found the following files/directories:
  ''NULL''
```

This may be a bug with R-hub ([R-hub issue #560](https://github.com/r-hub/rhub/issues/560)) and may be ignored.

```
* checking for detritus in the temp directory ... NOTE
Found the following files/directories:
  'lastMiKTeXException'
```

This may be a bug in MiKTeX ([R-hub issue #503](https://github.com/r-hub/rhub/issues/503)) and may be ignored.
